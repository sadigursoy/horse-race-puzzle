<?php

namespace HorseRace;

use HorseRace\Events\RaceCreated;
use HorseRace\Exceptions\MaxRaceNumberReachedException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Race extends Model
{
    const MAX_NUM_RACES = 3;

    public $timestamps = false;

    protected $fillable = ['current_seconds', 'completed'];

    /**
     * Horse relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function horses()
    {
        return $this->hasMany(Horse::class);
    }

    /**
     * Running horses of the race
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function runningHorses()
    {
        return $this->hasMany(Horse::class)->whereNull('race_completion_seconds')
            ->join('horse_positions', 'horses.id', '=', 'horse_positions.horse_id')
            ->orderByDesc('horse_positions.current_position');
    }


    /**
     * Horses that finished the race
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function finishedHorses()
    {
        return $this->hasMany(Horse::class)->whereNotNull('race_completion_seconds')->orderBy('race_completion_seconds');
    }

    /**
     * Top 3 horse of the race
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function topFinishedHorses()
    {
        return $this->finishedHorses()->take(3);
    }

    /**
     * Race progress actions
     *
     * If race has horses running, increases current_seconds by 10
     * If all horses finished running, marks the race as completed.
     */
    public function progressRace()
    {
        if (count($this->runningHorses) > 0) { // Still running horses exist, race is going on..
            $this->update([
                'current_seconds' => $this->current_seconds += 10
            ]);
        } else { // There's no running horses left for the race
            $this->update([
                'current_seconds' => null,
                'completed' => true
            ]);
        }
    }

    /**
     * Returns all active races.
     *
     * @return Race[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function activeRaces()
    {
        return Race::where('completed', false)->get();
    }

    /**
     * Returns last 5 completed races.
     *
     * @return Race[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function last5Races()
    {
        return Race::where('completed', true)->get()->sortByDesc('id')->take(5);
    }


    /**
     * @return Race
     * @throws MaxRaceNumberReachedException
     */
    public function createRace(): Race
    {
        if (count(Race::activeRaces()) === static::MAX_NUM_RACES) {
            throw new MaxRaceNumberReachedException();
        } else {
            $race = Race::create();

            event(new RaceCreated($race));

            return $race;
        }
    }
}
