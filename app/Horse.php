<?php

namespace HorseRace;

use HorseRace\Events\HorseCreated;
use Illuminate\Database\Eloquent\Model;
use HorseRace\HorsePosition;

class Horse extends Model
{

    public $timestamps = false;

    protected $guarded = [];


    /**
     * Horse Position relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function horsePosition()
    {
        return $this->hasOne(HorsePosition::class);
    }

    /**
     * Race relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function race()
    {
        return $this->belongsTo(Race::class);
    }

    /**
     * Create 8 horses for the race
     * Speed, Strength and Endurance are randomly generated
     *
     * @param Race $race
     */
    public static function createHorses(Race $race)
    {
        for ($i = 0; $i < 8; $i++) {
            $horse = self::create([
                'race_id'    => $race->id,
                'speed'     => rand(0, 100) / 10,
                'strength'  => rand(0, 100) / 10,
                'endurance' => rand(0, 100) / 10
            ]);

            event(new HorseCreated($horse));
        }
    }

    /**
     * Returns the best horse ever
     *
     * @return Horse
     */
    public static function best()
    {
        return Horse::whereNotNull('race_completion_seconds')->orderBy('race_completion_seconds')->first();
    }
}
