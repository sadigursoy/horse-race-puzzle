<?php

namespace HorseRace\Exceptions;

use Exception;

class MaxRaceNumberReachedException extends Exception
{
    protected $message = 'Max 3 races can be active..';
}
