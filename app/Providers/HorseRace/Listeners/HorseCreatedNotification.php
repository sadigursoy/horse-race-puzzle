<?php

namespace HorseRace\Providers\HorseRace\Listeners;

use HorseRace\Providers\HorseRace\Events\HorseCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class HorseCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  HorseCreated  $event
     * @return void
     */
    public function handle(HorseCreated $event)
    {
        //
    }
}
