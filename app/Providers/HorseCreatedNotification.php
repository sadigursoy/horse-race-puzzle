<?php

namespace HorseRace\Providers;

use HorseRace\Providers\HorseCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class HorseCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  HorseCreated  $event
     * @return void
     */
    public function handle(HorseCreated $event)
    {
        //
    }
}
