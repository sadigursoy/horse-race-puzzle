<?php

namespace HorseRace\Providers;

use HorseRace\Events\RaceCreated;
use HorseRace\Listeners\RaceCreatedNotification;
use HorseRace\Events\HorseCreated;
use HorseRace\Listeners\HorseCreatedNotification;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        RaceCreated::class => [
            RaceCreatedNotification::class
        ],
        HorseCreated::class => [
            HorseCreatedNotification::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
