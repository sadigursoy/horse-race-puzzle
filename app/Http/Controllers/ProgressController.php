<?php

namespace HorseRace\Http\Controllers;

use HorseRace\Horse;
use HorseRace\HorsePosition;
use HorseRace\Race;

use Illuminate\Http\Request;

class ProgressController extends Controller
{
    /**
     * Progress method for Race
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function progress()
    {
        if (count(Race::activeRaces()) > 0) {
            foreach (Race::activeRaces() as $race) {
                // Get the running horses of the race
                foreach ($race->runningHorses as $horse) {
                    // Progress the horse one move forward.
                    HorsePosition::progressHorse($horse);
                }

                // Refresh the running horses list before race progressed function.
                $race = $race->fresh();

                // Do race progress related tasks.
                $race->progressRace();
            }

            return redirect('/')->with('success', 'Race(s) progressed.');
        } else {
            return redirect('/')->with('error', 'No active races.');
        }
    }
}
