<?php

namespace HorseRace\Http\Controllers;

use HorseRace\Exceptions\MaxRaceNumberReachedException;
use HorseRace\Horse;
use HorseRace\Race;
use Illuminate\Http\Request;

class RaceController extends Controller
{
    /**
     * Store method for the Race.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
//        Race::create($request->all());
        try {
            $race = Race::createRace($request);

            return redirect('/')->with('success', "Race #$race->id  created");
        } catch (MaxRaceNumberReachedException $maxRaceNumberReachedException) {
            return redirect('/')->with('error', $maxRaceNumberReachedException->getMessage());
        }
    }

    /**
     * List method for Race
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request)
    {
        $activeRaces = Race::activeRaces();

        return view('races', compact('activeRaces'));
    }

    /**
     * Show method for Race
     *
     * @param Race $race
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Race $race)
    {
        return view('race', compact('race'));
    }

    /**
     * Last 5 completed Race stats
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function stats()
    {
        $last5races = Race::last5Races();

        return view('stats', compact('last5races'));
    }

    /**
     * Best Horse stats
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function best()
    {
        $bestHorse = Horse::best();

        return view('best', compact('bestHorse'));
    }
}
