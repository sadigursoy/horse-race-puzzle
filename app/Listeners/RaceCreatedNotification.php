<?php

namespace HorseRace\Listeners;

use HorseRace\Events\RaceCreated;
use HorseRace\Horse;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RaceCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RaceCreated  $event
     * @return void
     */
    public function handle(RaceCreated $event)
    {
        Horse::createHorses($event->race);
    }
}
