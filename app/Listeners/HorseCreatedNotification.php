<?php

namespace HorseRace\Listeners;

use HorseRace\Events\HorseCreated;
use HorseRace\HorsePosition;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class HorseCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  HorseCreated  $event
     * @return void
     */
    public function handle(HorseCreated $event)
    {
        HorsePosition::setInitialPosition($event->horse);
    }
}
