<?php

namespace HorseRace;

use Illuminate\Database\Eloquent\Model;
use HorseRace\Horse;

class HorsePosition extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function horse()
    {
        return $this->belongsTo(Horse::class);
    }


    /**
     * Sets initial position of the horse when created.
     *
     * Rule: A horse's speed is their base speed (5 m/s) + their speed stat (in m/s)
     *
     * @param \HorseRace\Horse $horse
     */
    public static function setInitialPosition(Horse $horse)
    {
        self::create([
            'horse_id'    => $horse->id,
            'initial_speed' => 5 + $horse->speed,
            'current_speed' => 5 + $horse->speed
        ]);
    }

    /**
     * Progress the horse one move forward
     *
     * @param \HorseRace\Horse $horse
     */
    public static function progressHorse(Horse $horse)
    {
        $positionAfterMove = self::calculatePositionAfterMove($horse);

        if ($positionAfterMove > 1500) { // Horse finishes the race
            $horse->update([
                'race_completion_seconds' => $horse->race->current_seconds + self::calculateTimeToFinishRace($horse)
            ]);

            $horse->horsePosition()->delete();
        } else { // Horse will be still running
            $horse->horsePosition->update([
                'current_position' => $positionAfterMove,
                'current_speed' => (static::willSlowDown($horse)) ? static::calculateSpeedAfterEndurance($horse) : $horse->horsePosition->current_speed,
            ]);
        }
    }

    /**
     * Check if horse will slow down within this move iteration.
     * If horse passes its endurance; it will slow down
     * and all calculations (final position and speed) is related to this.
     *
     * @param \HorseRace\Horse $horse
     * @return bool
     */
    protected static function willSlowDown(Horse $horse): bool
    {
        if ($horse->endurance * 100 > 1500) { // The horse may have enough power to end the race without slowing down.
            return false;
        } else {
            if ($horse->horsePosition->current_speed <> $horse->horsePosition->initial_speed) { // Horse already passed the endurance limit and slowed down
                return false;
            } else { // Not slowed down yet. Now?
                return $horse->horsePosition->current_position + (10 * $horse->horsePosition->current_speed) >= $horse->endurance * 100;
            }
        }
    }

    /**
     * Calculate the possible max position for the horse after this move.
     *
     * (If position is calculated bigger than 1.500 m; the race will be finished for that horse)
     *
     * @param \HorseRace\Horse $horse
     * @return float
     */
    protected static function calculatePositionAfterMove(Horse $horse)
    {
        if (! static::willSlowDown($horse)) { // Speed won't change.. Final position (and speed) depends on current values only.
            return  $horse->horsePosition->current_position +
                    $horse->horsePosition->current_speed * 10;
        } else {
            $distanceWithCurrentSpeed = $horse->endurance * 100 - $horse->horsePosition->current_position;
            $timePassedWithCurrentSpeed = $distanceWithCurrentSpeed / $horse->horsePosition->current_speed;
            $remainingTimeWithNewSpeed = 10 - $timePassedWithCurrentSpeed;
            $distanceWithNewSpeed = $remainingTimeWithNewSpeed * self::calculateSpeedAfterEndurance($horse);

            return  $horse->horsePosition->current_position +
                    $distanceWithCurrentSpeed +
                    $distanceWithNewSpeed;
        }
    }

    /**
     * Calculate time needed for the horse to finish the race within this move
     * (If position is calculated bigger than 1.500 m for a specific move; the race will be finished for that horse)
     *
     * @param \HorseRace\Horse $horse
     * @return float|int
     */
    protected static function calculateTimeToFinishRace(Horse $horse)
    {
        if (! static::willSlowDown($horse)) { // Speed won't change.. Return time needed to complete the race with current speed
            $remainingDistance = 1500 - $horse->horsePosition->current_position;

            return  $remainingDistance / $horse->horsePosition->current_speed;
        } else {
            $distanceWithCurrentSpeed = $horse->endurance * 100 - $horse->horsePosition->current_position;
            $timePassedWithCurrentSpeed = $distanceWithCurrentSpeed / $horse->horsePosition->current_speed;
            $remainingDistance = 1500 - $horse->endurance * 100;
            $timePassedWithNewSpeed = $remainingDistance / self::calculateSpeedAfterEndurance($horse);

            return  $timePassedWithCurrentSpeed + $timePassedWithNewSpeed;
        }
    }

    /**
     * Calculate the speed of the horse after endurance.
     *
     * Rule: A  jockey slows the horse down by 5 m/s, but this effect is reduced by the horse's strength * 8 as a percentage
     *
     * @param \HorseRace\Horse $horse
     * @return float|int
     */
    protected static function calculateSpeedAfterEndurance(Horse $horse)
    {
        $speedDecrease = 5 * (1 - $horse->strength * 8 / 100);
        return $horse->horsePosition->initial_speed - $speedDecrease;
    }
}
