@extends('layouts.main')

@section('content')


    <div class="row" style="padding: 10px 0 10px 0">
        <div class="col-md-3">
            <form action="{{ url('race') }}" method="POST">
                @csrf
                <button type="submit" class="btn btn-outline-primary">Create Race</button>
            </form>
        </div>
        <div class="col-md-3">
            <form action="{{ url('progress') }}" method="POST">
                @csrf
                <button type="submit" class="btn btn-outline-info">Progress Races</button>
            </form>
        </div>
    </div>



    @include('errors')

    @if(isset($activeRaces))

        <ul class="list-group">

            @foreach($activeRaces as $race)
                <li class="list-group-item">
                    <a href="/race/{{ $race->id }}">
                    Race #{{ $race->id }}
                    </a>
                    is at {{ $race->current_seconds }}<sup>th</sup> second
                    with {{ count($race->runningHorses) }} horses
                </li>
            @endforeach

        </ul>
    @else
        <p>
            No races.
        </p>
    @endif




@endsection()