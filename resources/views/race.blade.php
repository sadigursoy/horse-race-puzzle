@extends('layouts.main')

@section('content')

<h1> Race #{{ $race->id }} </h1>

@if(!is_null($race->current_seconds))
    <h2> Current time: {{ $race->current_seconds }} </h2>
@endif

<?php
$ranking = 0;
?>

@if(count($race->finishedHorses) > 0)
    <table class="table ">
        <thead>
        <tr>
            <th scope="col" colspan="3">Completed..</th>
        </tr>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Horse #</th>
            <th scope="col">Race completion seconds</th>
        </tr>
        </thead>

        <tbody>
        @foreach($race->finishedHorses as $horse)
            <tr>
                <th scope="row">{{  ++$ranking }}</th>
                <td>Horse #{{ $horse->id }}</td>
                <td>{{ $horse->race_completion_seconds }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif


@if(count($race->runningHorses) > 0)
    <table class="table">
        <thead>
        <tr>
            <th scope="col" colspan="3">Running..</th>
        </tr>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Horse #</th>
            <th scope="col">Current position</th>
            <th scope="col">Current speed</th>
        </tr>
        </thead>

        <tbody>
        @foreach($race->runningHorses as $horse)
            <tr>
                <th scope="row">{{  ++$ranking }}</th>
                <td>Horse #{{ $horse->id }}</td>
                <td>{{ $horse->current_position }}</td>
                <td>{{ $horse->current_speed }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@include('errors')

@endsection()