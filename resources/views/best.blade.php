@extends('layouts.main')

@section('content')


@if(! $bestHorse)
    <p>
    No horse has completed race
    </p>
@else
    <h2>Best Horse: Horse #{{ $bestHorse->id }}</h2>

    <h3>Completed 1.500 meters at {{ $bestHorse->race_completion_seconds }} seconds.</h3>

    <table class="table ">
        <thead>
        <tr>
            <th scope="col">Race #</th>
            <th scope="col">Speed</th>
            <th scope="col">Strength</th>
            <th scope="col">Endurance</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><a href="/race/{{ $bestHorse->race->id }}">Race #{{ $bestHorse->race->id }}</a></td>
                <td>{{ $bestHorse->speed }}</td>
                <td>{{ $bestHorse->strength }}</td>
                <td>{{ $bestHorse->endurance }}</td>

            </tr>
        </tbody>
    </table>

@endif

@endsection()
