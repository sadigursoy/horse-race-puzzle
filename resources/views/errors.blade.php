@if(session()->has('error') && session('error') <> '' )
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif

@if(session()->has('success') && session('success') <> '' )
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

