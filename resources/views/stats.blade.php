@extends('layouts.main')

@section('content')

    <h2>Last races</h2>

@if(count($last5races) == 0)
<p>
    No completed races found
</p>
@else

    @foreach($last5races as $race)

        <?php
        $ranking = 0;
        ?>

        <h3>Race {{ $race->id }}</h3>

        <table class="table ">
            <thead>
            <tr>
                <th scope="col">Position</th>
                <th scope="col">Horse #</th>
                <th scope="col">Race completion seconds</th>
            </tr>
            </thead>

            <tbody>
            @foreach($race->topFinishedHorses as $horse)
                <tr>
                    <th scope="row">{{  ++$ranking }}</th>
                    <td>Horse #{{ $horse->id }}</td>
                    <td>{{ $horse->race_completion_seconds }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    @endforeach

@endif

@endsection()