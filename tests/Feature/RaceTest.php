<?php

namespace Tests\Feature;

use Tests\TestCase;
use HorseRace\Race;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class RaceTest extends TestCase
{
    use RefreshDatabase;

    public function testHomePageReturns200()
    {
        $response = $this->call('get', '/');
        $response->assertStatus(200);
    }

    public function testRaceListReturns404ForNonExistingRace()
    {
        $response = $this->call('get', '/race/1');
        $response->assertStatus(404);
    }

    public function testRaceListReturnsDataForExistingRace()
    {
        $race = (new Race())->createRace();

        $response = $this->call('get', '/race/1');
        $response->assertStatus(200);
    }
}
