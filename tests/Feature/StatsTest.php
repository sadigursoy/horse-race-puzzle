<?php

namespace Tests\Feature;

use Tests\TestCase;
use HorseRace\Race;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class StatsTest extends TestCase
{
    use RefreshDatabase;

    public function testStatsPageReturns200()
    {
        $response = $this->call('get', '/stats');
        $response->assertStatus(200);
    }
}
