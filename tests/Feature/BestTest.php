<?php

namespace Tests\Feature;

use Tests\TestCase;
use HorseRace\Race;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class BestTest extends TestCase
{
    use RefreshDatabase;

    public function testBestPageReturns200()
    {
        $response = $this->call('get', '/best');
        $response->assertStatus(200);
    }
}
