<?php

namespace Tests\Feature;

use Tests\TestCase;
use HorseRace\Race;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ProgressTest extends TestCase
{
    use RefreshDatabase;

    public function testProgressRequestReturnsErrorWhenNoRaceActive()
    {
        $this->withoutMiddleware();

        $response = $this->call('post', '/progress');
        $response->assertSessionHas('error');
    }

    public function testProgressRequestReturnsSuccessWhenRaceActiveExist()
    {
        $this->withoutMiddleware();

        $race = (new Race())->createRace();

        $response = $this->call('post', '/progress');
        $response->assertSessionHas('success');
    }
}
