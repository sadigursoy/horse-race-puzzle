<?php

namespace Tests\Unit;

use HorseRace\Horse;
use HorseRace\Race;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HorseTest extends TestCase
{
    use RefreshDatabase;

    /** @var Race  */
    private $race;

    protected function setUp(): void
    {
        parent::setUp();

        $this->race = (new Race())->createRace();
    }

    public function test8HorsesAreCreatedWhenRaceIsCreated()
    {
        $this->assertCount(8, $this->race->horses);
    }

    public function testHorsesHavePositionsWhenNewlyCreated()
    {
        Horse::all()->each(function (Horse $horse) {
            $this->assertNotEmpty($horse->horsePosition());
        });
    }

    public function testHorsesBelongToRace()
    {
        Horse::all()->each(function (Horse $horse) {
            $this->assertNotEmpty($horse->race());
        });
    }

    public function testBestReturnsEmptyWhenNoHorsesFinished()
    {
        $this->assertEmpty(Horse::best());
    }

    public function testBestReturnsAHorseWhenSomeHorsesFinished()
    {
        Horse::first()->update([
            'race_completion_seconds' => 1
        ]);

        $this->assertNotEmpty(Horse::best());
    }

    public function testBestReturnsBestHorse()
    {
        Horse::where('id', 1)->update([
            'race_completion_seconds' => 1
        ]);

        Horse::where('id', 2)->update([
            'race_completion_seconds' => 2
        ]);

        $this->assertEquals(1, Horse::best()->id);
    }
}
