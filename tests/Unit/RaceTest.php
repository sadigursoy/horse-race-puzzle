<?php

namespace Tests\Unit;

use HorseRace\Horse;
use HorseRace\Race;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RaceTest extends TestCase
{
    use RefreshDatabase;

    /** @var Race  */
    private $race;

    protected function setUp(): void
    {
        parent::setUp();

        $this->race = (new Race())->createRace();
    }

    public function testRaceCanBeCreated()
    {
        $this->assertSame($this->race->id, 1);
    }

    public function test8HorsesAreRunningWhenRaceIsCreated()
    {
        $this->assertCount(8, $this->race->runningHorses);
    }

    public function testNoHorsesFinishedWhenRaceIsCreated()
    {
        $this->assertEmpty($this->race->finishedHorses);
    }

    public function testTopFinishedHorsesReturnsEmptyWhenNoHorseFinishedRace()
    {
        $this->assertEmpty($this->race->topFinishedHorses);
    }

    public function testRunningAndFinishedHorsesCountsSumUpTo8()
    {
        $this->race->runningHorses->first()->update([
            'race_completion_seconds' => 1
        ]);

        $race = $this->race->refresh();

        $runningHorseCount = $this->race->runningHorses->count();
        $finishedHorseCount = $this->race->finishedHorses->count();

        $this->assertSame(8, $runningHorseCount + $finishedHorseCount);
    }

    public function testTopFinishedHorsesReturnsNotEmptyWhenAnyHorseFinishedRace()
    {
        $this->race->runningHorses->first()->update([
            'race_completion_seconds' => 1
        ]);

        $race = $this->race->refresh();

        $this->assertNotEmpty($this->race->topFinishedHorses);
    }

    public function testProgressRaceDoesActionWithRunningHorses()
    {
        $this->race->progressRace();

        $this->assertEquals(10, $this->race->current_seconds);
    }

    public function testProgressRaceDoesNothingWithNoRunningHorses()
    {
        $currentSecondsBeforeProgress = $this->race->current_seconds;

        Horse::all()->each(function (Horse $horse) {
            $horse->update([
               'race_completion_seconds' => 1
            ]);
        });

        $this->race->progressRace();

        $currentSecondsAfterProgress = $this->race->current_seconds;

        $this->assertEquals($currentSecondsBeforeProgress, $currentSecondsAfterProgress);
    }

    public function testLast5RacesReturns0WhenNoRaceIsCompleted()
    {
        $this->assertCount(0, Race::last5Races());
    }

    public function testLast5RacesReturns1When1RaceIsCompleted()
    {
        $this->race->update([
            'completed' => true
        ]);
        $this->assertCount(1, Race::last5Races());
    }

    public function testLast5RacesReturnsMaxRaces()
    {
        $race = factory(Race::class, 6)->create();
        Race::all()->each(function (Race $race) {
            $race->update([
                'completed' => true
            ]);
        });
        $this->assertCount(5, Race::last5Races());
    }
}
