<?php

namespace Tests\Unit;

use HorseRace\Horse;
use HorseRace\HorsePosition;
use HorseRace\Race;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HorsePositionTest extends TestCase
{
    use RefreshDatabase;

    /** @var Race */
    private $race;

    protected function setUp(): void
    {
        parent::setUp();

        $this->race = (new Race())->createRace();
    }

    public function testHorsesHaveInitialPosition()
    {
        HorsePosition::all()->each(function (HorsePosition $horsePosition) {
            $this->assertEquals($horsePosition->horse->speed + 5, $horsePosition->initial_speed);
            $this->assertEquals($horsePosition->horse->speed + 5, $horsePosition->current_speed);
        });
    }

    public function testHorseCanFinishRace()
    {
        $horsePosition = HorsePosition::first();

        $horsePosition->update([
            'current_position' => 1499,
            'current_speed' => 100
        ]);

        HorsePosition::progressHorse($horsePosition->horse);

        $this->assertNotEmpty($horsePosition->horse->race_completion_seconds);
    }

    public function testHorseCanMove()
    {
        $horsePosition = HorsePosition::first();

        $horsePosition->horse->endurance = 2000;

        $horsePosition->update([
            'current_position' => 500,
            'current_speed' => 100

        ]);

        HorsePosition::progressHorse($horsePosition->horse);

        $horsePosition->refresh();

        $this->assertEquals($horsePosition->current_position, 1500);
    }

    public function testWillSlowDownDetectsUnlimitedEndurance()
    {
        $horsePosition = HorsePosition::first();

        $horsePosition->horse->endurance = 2000;

        $this->assertFalse(HorsePosition::willSlowDown($horsePosition->horse));
    }

    public function testWillSlowDownDetectsAlreadyChangedSpeed()
    {
        $horsePosition = HorsePosition::first();

        $horsePosition->update([
            'initial_speed' => 2,
            'current_speed' => 2
        ]);

        $horsePosition->horse->update([
           'endurance' => 100
        ]);
        $horsePosition->horse->refresh();

        $this->assertFalse(HorsePosition::willSlowDown($horsePosition->horse));
    }

    public function testWillSlowDownDetectsPassingEndurance()
    {
        $horsePosition = HorsePosition::first();

        $horsePosition->horse->update([
            'endurance' => 0
        ]);
        $horsePosition->horse->refresh();

        $this->assertTrue(HorsePosition::willSlowDown($horsePosition->horse));
    }

    public function testSpeedAfterEnduranceIsCorrect()
    {
        $horsePosition = HorsePosition::first();

        $horsePosition->update([
            'initial_speed' => 10
        ]);

        $horsePosition->horse->update([
            'strength' => 3.2,
        ]);
        $horsePosition->horse->refresh();

        $horsePosition->refresh();

        $this->assertEquals(6.28, HorsePosition::calculateSpeedAfterEndurance($horsePosition->horse));
    }
}
