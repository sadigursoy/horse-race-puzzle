<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorsePositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horse_positions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('horse_id');
            $table->foreign('horse_id')->on('horses')->references('id');

            // Stats that change during race
            $table->double('initial_speed')->default(0.0);
            $table->double('current_speed')->default(0.0);
            $table->double('current_position')->default(0.0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horse_positions');
    }
}
