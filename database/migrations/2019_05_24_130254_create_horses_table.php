<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horses', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('race_id')->nullable(false);
            $table->foreign('race_id')->on('races')->references('id');

            // Initial stats
            $table->decimal('speed', 3, 1)->default(0.0);
            $table->decimal('strength', 3, 1)->default(0.0);
            $table->decimal('endurance', 3, 1)->default(0.0);

            $table->decimal('race_completion_seconds', 5, 1)->nullable(true);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horses');
    }
}
