<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Home page. Lists races
 */
Route::get('/', 'RaceController@list');

/**
 * Race actions.
 */
Route::post('/race', 'RaceController@store');
Route::get('/race/{race}', 'RaceController@show');

/**
 * Returns last completed race stats
 */
Route::get('/stats', 'RaceController@stats');


/**
 * Returns best horse
 */
Route::get('/best', 'RaceController@best');


/**
 * Progresses all active races
 */
Route::post('/progress', 'ProgressController@progress');

